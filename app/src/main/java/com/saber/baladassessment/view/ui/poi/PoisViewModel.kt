package com.saber.baladassessment.view.ui.poi

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.saber.baladassessment.data.entities.VenuesResponse
import com.saber.baladassessment.data.repository.PoiRepository
import com.saber.baladassessment.utils.Resource
import retrofit2.Response

class PoisViewModel @ViewModelInject constructor(
    private val repository: PoiRepository
) : ViewModel() {
    fun getVenues(lat: Double, lng: Double): LiveData<Resource<List<VenuesResponse>>> {
        return repository.getPois(lat,lng)
    }

//    val pois = repository.getPois()
}
