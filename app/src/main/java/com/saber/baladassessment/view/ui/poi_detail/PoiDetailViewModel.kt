package com.saber.baladassessment.view.ui.poi_detail

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import com.saber.baladassessment.data.entities.Poi
import com.saber.baladassessment.data.entities.poi_detail.PoiDetail
import com.saber.baladassessment.data.repository.PoiRepository
import com.saber.baladassessment.utils.Resource

class PoiDetailViewModel @ViewModelInject constructor(
    private val repository: PoiRepository
) : ViewModel() {

    private val _id = MutableLiveData<String>()

    private val _poi = _id.switchMap { id ->
        repository.getPoiDetail(id)
    }
    val poi: LiveData<Resource<PoiDetail>> = _poi


    fun start(id: String) {
        _id.value = id
    }

}
