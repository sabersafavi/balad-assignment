package com.saber.baladassessment.view.ui.poi

import android.app.PendingIntent
import android.content.Intent
import android.content.SharedPreferences
import android.location.Location
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.saber.baladassessment.R
import com.saber.baladassessment.databinding.PoisFragmentBinding
import com.saber.baladassessment.receiver.LocationUpdatesBroadcastReceiver
import com.saber.baladassessment.utils.LocationUtils
import com.saber.baladassessment.utils.autoCleared
import com.saber.baladassessment.view.adapters.PoisAdapter
import com.saber.baladassessment.view.ui.MainActivity
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class PoisFragment : Fragment(), PoisAdapter.PoiItemListener,
    SharedPreferences.OnSharedPreferenceChangeListener {

    private val TAG = MainActivity::class.java.simpleName
    private val UPDATE_INTERVAL: Long = 5000 // Every 60 seconds.
    private val FASTEST_UPDATE_INTERVAL: Long = 1000 // Every 30 seconds
    private val MAX_WAIT_TIME = UPDATE_INTERVAL * 5 // Every 5 minutes.

    private var mLocationRequest: LocationRequest? = null
    private var mFusedLocationClient: FusedLocationProviderClient? = null

    private var binding: PoisFragmentBinding by autoCleared()
    private val viewModel: PoisViewModel by viewModels()
    private lateinit var adapter: PoisAdapter

    lateinit var lastLocation: Location

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = PoisFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupObservers()
        //
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        createLocationRequest()

    }

    override fun onStart() {
        super.onStart()
        PreferenceManager.getDefaultSharedPreferences(requireContext())
            .registerOnSharedPreferenceChangeListener(this)
    }


    override fun onResume() {
        super.onResume()
        requestLocationUpdates(binding.progressBar)
    }

    override fun onPause() {
        super.onPause()
        removeLocationUpdates(binding.progressBar)
    }

    override fun onStop() {
        PreferenceManager.getDefaultSharedPreferences(requireContext())
            .unregisterOnSharedPreferenceChangeListener(this)
        super.onStop()
    }


    private fun setupRecyclerView() {
        adapter = PoisAdapter(this)
        binding.charactersRv.layoutManager = LinearLayoutManager(requireContext())
        binding.charactersRv.adapter = adapter
    }

    private fun setupObservers() {
//        viewModel.characters.observe(viewLifecycleOwner, Observer {
//            when (it.status) {
//                Resource.Status.SUCCESS -> {
//                    binding.progressBar.visibility = View.GONE
//                    if (!it.data.isNullOrEmpty()) adapter.setItems(ArrayList(it.data))
//                }
//                Resource.Status.ERROR ->
//                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
//
//                Resource.Status.LOADING ->
//                    binding.progressBar.visibility = View.VISIBLE
//            }
//        })
    }

    override fun onClicked(id: String) {
        findNavController().navigate(
            R.id.action_charactersFragment_to_characterDetailFragment,
            bundleOf("id" to id)
        )
    }

    override fun onSharedPreferenceChanged(
        sharedPreferences: SharedPreferences?,
        s: String
    ) {
        if (s == LocationUtils.KEY_LOCATION_UPDATES_RESULT) {

            Toast.makeText(requireContext(), LocationUtils.getLocationUpdatesResult(requireContext()), Toast.LENGTH_SHORT).show()
        }
    }

    //////

    private fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.interval = UPDATE_INTERVAL
        mLocationRequest!!.fastestInterval = FASTEST_UPDATE_INTERVAL
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest!!.maxWaitTime = MAX_WAIT_TIME
    }

    private fun getPendingIntent(): PendingIntent? {
        val intent = Intent(requireContext(), LocationUpdatesBroadcastReceiver::class.java)
        intent.action = LocationUpdatesBroadcastReceiver.ACTION_PROCESS_UPDATES
        return PendingIntent.getBroadcast(requireContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    fun requestLocationUpdates(view: View?) {
        try {

            mFusedLocationClient!!.lastLocation.addOnSuccessListener {
                    lastLocation = it
                    getVenues(lastLocation)
            }
            Log.i(TAG, "Starting location updates")
            LocationUtils.setRequestingLocationUpdates(requireContext(), true)
            mFusedLocationClient!!.requestLocationUpdates(mLocationRequest, getPendingIntent())
        } catch (e: SecurityException) {
            LocationUtils.setRequestingLocationUpdates(requireContext(), false)
            e.printStackTrace()
        }
    }

    private fun getVenues(ll: Location?) {
        viewModel.getVenues(ll!!.latitude, ll!!.longitude).observe(this, Observer {
            if (!it.data.isNullOrEmpty()) adapter.setItems(ArrayList(it.data[0].venues))
        })
    }

    fun removeLocationUpdates(view: View?) {
        Log.i(TAG, "Removing location updates")
        LocationUtils.setRequestingLocationUpdates(requireContext(), false)
        mFusedLocationClient!!.removeLocationUpdates(getPendingIntent())
    }
}
