package com.saber.baladassessment.view.ui.poi_detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.saber.baladassessment.data.entities.poi_detail.PoiDetail
import com.saber.baladassessment.databinding.PoiDetailFragmentBinding
import com.saber.baladassessment.utils.Resource
import com.saber.baladassessment.utils.autoCleared
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PoiDetailFragment : Fragment() {

    private var binding: PoiDetailFragmentBinding by autoCleared()
    private val viewModel: PoiDetailViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = PoiDetailFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getString("id", "562903cf498ea7296901f36d")?.let { viewModel.start(it) }
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.poi.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    if(it.data!=null) {
                        bindCharacter(it.data!!)
                        binding.progressBar.visibility = View.GONE
                        binding.characterCl.visibility = View.VISIBLE
                    }
                }

                Resource.Status.ERROR ->
                    Toast.makeText(activity, it.message, Toast.LENGTH_SHORT).show()

                Resource.Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.characterCl.visibility = View.GONE
                }
            }
        })
    }

    private fun bindCharacter(detail: PoiDetail) {
        binding.name.text = detail.name
        binding.tvS1.text = detail.location.address
        binding.tvS2.text = detail.shortUrl
//        binding.gender.text = detail.gender
//        Glide.with(binding.root)
//            .load(character.image)
//            .transform(CircleCrop())
//            .into(binding.image)
    }
}
