package com.saber.baladassessment.view.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.saber.baladassessment.data.entities.Poi
import com.saber.baladassessment.data.entities.VenuesResponse
import com.saber.baladassessment.databinding.ItemPoiBinding

class PoisAdapter(private val listener: PoiItemListener) :
    RecyclerView.Adapter<PoiViewHolder>() {

    interface PoiItemListener {
        fun onClicked(id: String)
    }

    private val items = ArrayList<Poi>()

    fun setItems(items: ArrayList<Poi>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PoiViewHolder {
        val binding: ItemPoiBinding =
            ItemPoiBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PoiViewHolder(binding, listener)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: PoiViewHolder, position: Int) =
        holder.bind(items[position])
}

class PoiViewHolder(
    private val itemBinding: ItemPoiBinding,
    private val listener: PoisAdapter.PoiItemListener
) : RecyclerView.ViewHolder(itemBinding.root),
    View.OnClickListener {

    private lateinit var poi: Poi

    init {
        itemBinding.root.setOnClickListener(this)
    }

    @SuppressLint("SetTextI18n")
    fun bind(item: Poi) {
        this.poi = item
        itemBinding.name.text = item.name
        if(item.categories.isNotEmpty()){
            itemBinding.speciesAndStatus.text = item.categories[0].name
        } else {
            itemBinding.speciesAndStatus.text = """${item.referralId} - ${item.hasPerk}"""
        }
//        Glide.with(itemBinding.root)
//            .load(item.)
//            .transform(CircleCrop())
//            .into(itemBinding.image)
    }

    override fun onClick(v: View?) {
        listener.onClicked(poi.id)
    }
}

