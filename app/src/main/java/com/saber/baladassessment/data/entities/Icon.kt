package com.saber.baladassessment.data.entities

data class Icon(
    val prefix: String,
    val suffix: String
) {
    override fun toString(): String {
        return prefix + suffix
    }
}