package com.saber.baladassessment.data.entities.poi_detail

data class Item(
    val displayName: String,
    val displayValue: String,
    val priceTier: Int
)