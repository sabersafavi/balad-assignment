package com.saber.baladassessment.data.entities.poi_detail

data class Open(
    val renderedTime: String
)