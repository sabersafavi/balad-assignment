package com.saber.baladassessment.data.entities.poi_detail

data class Colors(
    val algoVersion: Int,
    val highlightColor: HighlightColor,
    val highlightTextColor: HighlightColor
)