package com.saber.baladassessment.data.entities.poi_detail

data class Listed(
    val count: Int,
    val groups: List<Group>
)