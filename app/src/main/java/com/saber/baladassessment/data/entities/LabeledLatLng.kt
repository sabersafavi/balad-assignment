package com.saber.baladassessment.data.entities

data class LabeledLatLng(
    val label: String,
    val lat: Double,
    val lng: Double
)