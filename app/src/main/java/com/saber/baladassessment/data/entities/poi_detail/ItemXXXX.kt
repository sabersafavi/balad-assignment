package com.saber.baladassessment.data.entities.poi_detail

data class ItemXXXX(
    val reasonName: String,
    val summary: String,
    val type: String
)