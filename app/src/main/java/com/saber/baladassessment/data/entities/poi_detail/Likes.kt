package com.saber.baladassessment.data.entities.poi_detail

data class Likes(
    val count: Int,
    val groups: List<Group>,
    val summary: String
)