package com.saber.baladassessment.data.entities.poi_detail

data class ItemXX(
    val createdAt: Int,
    val id: String,
    val photo: Photo
)