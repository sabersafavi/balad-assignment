package com.saber.baladassessment.data.remote

import com.saber.baladassessment.data.entities.GenericResponse
import com.saber.baladassessment.data.entities.PoiDetailResponse
import com.saber.baladassessment.data.entities.VenuesResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface FoursquareService {
    @GET("venues/search")
    suspend fun getVenues(
        @Query("client_id") client_id: String,
        @Query("client_secret") client_secret: String,
        @Query("v") v: String,
        @Query("ll") ll: String,
        @Query("intent") intent: String,
        @Query("radius") radius: String,
        @Query("limit") limit: String
    ): Response<GenericResponse<VenuesResponse>>

    @GET("venues/{venueId}")
    suspend fun getVenueDetail(
        @Path("venueId") id: String,
        @Query("client_id") client_id: String,
        @Query("client_secret") client_secret: String,
        @Query("v") v: String
    ): Response<GenericResponse<PoiDetailResponse>>
}