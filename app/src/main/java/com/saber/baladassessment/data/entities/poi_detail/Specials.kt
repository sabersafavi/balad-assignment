package com.saber.baladassessment.data.entities.poi_detail

data class Specials(
    val count: Int,
    val items: List<Any>
)