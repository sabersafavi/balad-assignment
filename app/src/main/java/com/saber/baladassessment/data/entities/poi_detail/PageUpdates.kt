package com.saber.baladassessment.data.entities.poi_detail

data class PageUpdates(
    val count: Int,
    val items: List<Any>
)