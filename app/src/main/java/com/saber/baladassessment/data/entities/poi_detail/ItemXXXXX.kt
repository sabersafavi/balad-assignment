package com.saber.baladassessment.data.entities.poi_detail

data class ItemXXXXX(
    val agreeCount: Int,
    val canonicalUrl: String,
    val createdAt: Int,
    val disagreeCount: Int,
    val id: String,
    val lang: String,
    val likes: Likes,
    val logView: Boolean,
    val text: String,
    val todo: Todo,
    val type: String,
    val user: User
)