package com.saber.baladassessment.data.entities.poi_detail

data class Menu(
    val anchor: String,
    val label: String,
    val mobileUrl: String,
    val type: String,
    val url: String
)