package com.saber.baladassessment.data.entities.poi_detail

data class HereNow(
    val count: Int,
    val groups: List<Any>,
    val summary: String
)