package com.saber.baladassessment.data.entities.poi_detail

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.saber.baladassessment.data.entities.Category
import com.saber.baladassessment.data.entities.Location
import com.saber.baladassessment.data.entities.VenuePage


@Entity(tableName = "venueDetails")
data class PoiDetail(
    @PrimaryKey
    val id: String,
    val name: String,
    val location: Location,
    val venuePage: VenuePage?,
    val categories: List<Category>,
    val allowMenuUrlEdit: Boolean,
    val attributes: Attributes?,
    val beenHere: BeenHere?,
    val bestPhoto: BestPhoto?,
    val canonicalUrl: String,
    val colors: Colors?,
    val contact: Contact?,
    val createdAt: Int,
    val defaultHours: DefaultHours?,
    val description: String?,
    val dislike: Boolean,
    val hasMenu: Boolean,
    val hereNow: HereNow?,
    val hours: Hours?,
    val inbox: Inbox?,
    val likes: Likes?,
    val listed: Listed?,
    val menu: Menu?,
    val ok: Boolean,
    val page: Page?,
    val pageUpdates: PageUpdates?,
    val photos: Photos?,
    val popular: Popular?,
    val price: Price?,
    val rating: Double,
    val ratingColor: String?,
    val ratingSignals: Int,
    val reasons: Reasons?,
    val shortUrl: String?,
    val specials: Specials?,
    val stats: Stats?,
    val timeZone: String?,
    val tips: TipsX?,
    val url: String?,
    val verified: Boolean
)