package com.saber.baladassessment.data.entities.poi_detail

data class HighlightColor(
    val photoId: String,
    val value: Int
)