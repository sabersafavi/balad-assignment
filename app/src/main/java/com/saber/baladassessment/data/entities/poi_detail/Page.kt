package com.saber.baladassessment.data.entities.poi_detail

data class Page(
    val user: User
)