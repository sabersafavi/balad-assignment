package com.saber.baladassessment.data.repository

import com.example.rickandmorty.utils.performGetOperation
import com.saber.baladassessment.data.local.PoiDao
import com.saber.baladassessment.data.remote.PoiRemoteDataSource
import javax.inject.Inject

class PoiRepository @Inject constructor(
    private val remoteDataSource: PoiRemoteDataSource,
    private val localDataSource: PoiDao
) {

    fun getPois(lat: Double, lng: Double) = performGetOperation(
        databaseQuery = {
            localDataSource.findByDistance(lat, lng)
        },
        networkCall = {
            remoteDataSource.getVenues(lat, lng)
        },
        saveCallResult = {
            it.response.lat = lat
            it.response.lng = lng
            localDataSource.insert(it.response)
        }
    )

    fun getPoiDetail(id: String) = performGetOperation(
        databaseQuery = {
            localDataSource.getPoiDetail(id)
        },
        networkCall = {
            remoteDataSource.getVenueDetail(id)
        },
        saveCallResult = {
            localDataSource.insert(it.response.venue)
        }
    )
}