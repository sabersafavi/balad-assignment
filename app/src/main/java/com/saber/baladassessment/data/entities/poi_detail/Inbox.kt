package com.saber.baladassessment.data.entities.poi_detail

data class Inbox(
    val count: Int,
    val items: List<Any>
)