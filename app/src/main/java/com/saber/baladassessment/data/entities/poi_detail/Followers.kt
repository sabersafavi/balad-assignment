package com.saber.baladassessment.data.entities.poi_detail

data class Followers(
    val count: Int
)