package com.saber.baladassessment.data.entities.poi_detail

data class Group(
    val count: Int,
    val items: List<Item>,
    val type: String,
    val name: String?,
    val summary: String?
)