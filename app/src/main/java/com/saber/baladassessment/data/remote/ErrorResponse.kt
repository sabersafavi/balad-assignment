package com.saber.baladassessment.data.remote

data class ErrorResponse(val msg: String)