package com.saber.baladassessment.data.entities.poi_detail

data class Hours(
    val dayData: List<Any>,
    val isLocalHoliday: Boolean,
    val isOpen: Boolean,
    val richStatus: RichStatus,
    val status: String,
    val timeframes: List<Timeframe>
)