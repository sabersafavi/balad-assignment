package com.saber.baladassessment.data.entities.poi_detail

data class ItemX(
    val canonicalUrl: String,
    val collaborative: Boolean,
    val createdAt: Int,
    val description: String,
    val editable: Boolean,
    val followers: Followers,
    val guide: Boolean,
    val guideType: String,
    val id: String,
    val listItems: ListItems,
    val logView: Boolean,
    val name: String,
    val photo: PhotoX,
    val `public`: Boolean,
    val readMoreUrl: String,
    val type: String,
    val updatedAt: Int,
    val url: String,
    val user: User
)