package com.saber.baladassessment.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.saber.baladassessment.data.entities.VenuesResponse
import com.saber.baladassessment.data.entities.poi_detail.PoiDetail
import com.saber.baladassessment.data.entities.typeConvertors.PoiConvertor

@Database(entities = [VenuesResponse::class, PoiDetail::class], version = 1, exportSchema = false)
@TypeConverters(PoiConvertor::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun poiDao(): PoiDao

    companion object {
        const val DATABASE_NAME = "venues"

        @Volatile private var instance: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase =
            instance ?: synchronized(this) { instance ?: buildDatabase(context).also { instance = it } }

        private fun buildDatabase(appContext: Context) =
            Room.databaseBuilder(appContext, AppDatabase::class.java, DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build()
    }

}