package com.saber.baladassessment.data.entities

data class GenericResponse<T> (
    var meta: Meta,
    var response: T
)