package com.saber.baladassessment.data.entities.poi_detail

data class ListItems(
    val count: Int,
    val items: List<ItemXX>
)