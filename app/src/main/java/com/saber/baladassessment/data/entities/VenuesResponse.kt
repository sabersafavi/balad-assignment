package com.saber.baladassessment.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "venues")
data class VenuesResponse(
    @PrimaryKey(autoGenerate = true)
    var id: Int,
    var venues: List<Poi>
) {
    var lat: Double = 0.0
    var lng: Double = 0.0
}