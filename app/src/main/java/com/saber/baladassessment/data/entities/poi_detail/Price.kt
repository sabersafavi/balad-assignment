package com.saber.baladassessment.data.entities.poi_detail

data class Price(
    val currency: String,
    val message: String,
    val tier: Int
)