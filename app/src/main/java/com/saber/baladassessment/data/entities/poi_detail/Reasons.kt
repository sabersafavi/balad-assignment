package com.saber.baladassessment.data.entities.poi_detail

data class Reasons(
    val count: Int,
    val items: List<ItemXXXX>
)