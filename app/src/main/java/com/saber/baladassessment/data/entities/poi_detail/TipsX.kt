package com.saber.baladassessment.data.entities.poi_detail

data class TipsX(
    val count: Int,
    val groups: List<Group>
)