package com.saber.baladassessment.data.entities.poi_detail

data class Source(
    val name: String,
    val url: String
)