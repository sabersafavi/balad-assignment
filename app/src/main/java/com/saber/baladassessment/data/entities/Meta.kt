package com.saber.baladassessment.data.entities

data class Meta (
    var code: Int,
    var requestId: String,
    val errorType: String?,
    val errorDetail: String?
)