package com.saber.baladassessment.data.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.saber.baladassessment.data.entities.VenuesResponse
import com.saber.baladassessment.data.entities.poi_detail.PoiDetail

@Dao
interface PoiDao {

    @Query("SELECT * FROM venues")
    fun getAllPois() : LiveData<List<VenuesResponse>>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(venues: List<VenuesResponse>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(venue: VenuesResponse)

    @Query("SELECT * FROM venues ORDER BY ABS(lat - :latitude) + ABS(lng - :longitude) ASC")
    fun findByDistance(latitude:Double,longitude:Double): LiveData<List<VenuesResponse>>

    //

    @Query("SELECT * FROM venueDetails WHERE id = :id")
    fun getPoiDetail(id: String): LiveData<PoiDetail>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(poi: PoiDetail)

}