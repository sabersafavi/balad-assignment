package com.saber.baladassessment.data.entities.poi_detail

data class Stats(
    val tipCount: Int
)