package com.saber.baladassessment.data.remote

import androidx.lifecycle.MutableLiveData
import com.saber.baladassessment.BuildConfig
import javax.inject.Inject

class PoiRemoteDataSource @Inject constructor(
    private val foursquareService: FoursquareService
) : BaseDataSource() {

    suspend fun getVenues(lat: Double, lng: Double) = getResult {
        foursquareService.getVenues(
            BuildConfig.client_id,
            BuildConfig.client_secret,
            BuildConfig.v,
            "$lat,$lng",
            QUERY_INTENT,
            RADIUS.toString(),
            LIMIT.toString()
        )

    }

    suspend fun getVenueDetail(id: String) = getResult {
        foursquareService.getVenueDetail(
            id,
            BuildConfig.client_id,
            BuildConfig.client_secret,
            BuildConfig.v
        )
    }

//    suspend fun getVenue(id: Int) = getResult { foursquareService.getCharacter(id) }


    companion object{
        val QUERY_INTENT = "browse"
        val RADIUS = 200
        val LIMIT = 1000
    }
}