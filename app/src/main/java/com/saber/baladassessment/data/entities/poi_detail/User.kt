package com.saber.baladassessment.data.entities.poi_detail

data class User(
    val countryCode: String,
    val firstName: String,
    val isSanctioned: Boolean,
    val type: String,
    val lastName: String?,
    val tips: Tips?,
    val lists: Lists?,
    val bio: String?,
    val venue: Venue?
)