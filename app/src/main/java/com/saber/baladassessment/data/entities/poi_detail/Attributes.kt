package com.saber.baladassessment.data.entities.poi_detail

data class Attributes(
    val groups: List<Group>
)