package com.saber.baladassessment.data.entities.poi_detail

data class Photos(
    val count: Int,
    val groups: List<Group>
)