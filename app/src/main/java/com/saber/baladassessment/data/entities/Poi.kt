package com.saber.baladassessment.data.entities

data class Poi(
    val id: String,
    val name: String,
    val location: Location,
    val categories: List<Category>,
    val referralId: String,
    val hasPerk: Boolean,
    val venuePage: VenuePage
)