package com.saber.baladassessment.data.entities

import com.saber.baladassessment.data.entities.poi_detail.PoiDetail

data class PoiDetailResponse(
    val venue: PoiDetail
) {
}