package com.saber.baladassessment.data.entities.typeConvertors

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.saber.baladassessment.data.entities.*
import com.saber.baladassessment.data.entities.poi_detail.*

class PoiConvertor {
    @TypeConverter
    fun fromPoi(value: Poi?): String? {
        if(value == null) return null
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toPoi(value: String?): Poi? {
        return Gson().fromJson(value, Poi::class.java)
    }

    //
    @TypeConverter
    fun fromLocation(value: Location?): String? {
        if(value == null) return null
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toLocation(value: String?): Location? {
        return Gson().fromJson(value, Location::class.java)
    }

    //
    @TypeConverter
    fun fromVenuePage(value: VenuePage?): String? {
        if(value == null) return null
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toVenuePage(value: String?): VenuePage? {
        return Gson().fromJson(value, VenuePage::class.java)
    }

    //
    @TypeConverter
    fun fromAttributes(value: Attributes?): String? {
        if(value == null) return null
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toAttributes(value: String?): Attributes? {
        return Gson().fromJson(value, Attributes::class.java)
    }

    //
    @TypeConverter
    fun fromBeenHere(value: BeenHere?): String? {
        if(value == null) return null
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toBeenHere(value: String?): BeenHere? {
        return Gson().fromJson(value, BeenHere::class.java)
    }

    //
    @TypeConverter
    fun fromBestPhoto(value: BestPhoto?): String? {
        if(value == null) return null
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toBestPhoto(value: String?): BestPhoto? {
        return Gson().fromJson(value, BestPhoto::class.java)
    }

    //
    @TypeConverter
    fun fromColors(value: Colors?): String? {
        if(value == null) return null
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toColors(value: String?): Colors? {
        return Gson().fromJson(value, Colors::class.java)
    }

    //
    @TypeConverter
    fun fromContact(value: Contact?): String? {
        if(value == null) return null
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toContact(value: String?): Contact? {
        return Gson().fromJson(value, Contact::class.java)
    }

    //
    @TypeConverter
    fun fromDefaultHours(value: DefaultHours?): String? {
        if(value == null) return null
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toDefaultHours(value: String?): DefaultHours? {
        return Gson().fromJson(value, DefaultHours::class.java)
    }

    //
    @TypeConverter
    fun fromOther(value: HereNow?): String? {
        if(value == null) return null
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toHereNow(value: String?): HereNow? {
        return Gson().fromJson(value, HereNow::class.java)
    }

    //
    @TypeConverter
    fun fromOther(value: Hours?): String? {
        if(value == null) return null
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toHours(value: String?): Hours? {
        return Gson().fromJson(value, Hours::class.java)
    }


    //
    @TypeConverter
    fun fromInbox(value: Inbox?): String? {
        if(value == null) return null
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toInbox(value: String?): Inbox? {
        return Gson().fromJson(value, Inbox::class.java)
    }


    //
    @TypeConverter
    fun fromLikes(value: Likes?): String? {
        if(value == null) return null
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toLikes(value: String?): Likes? {
        return Gson().fromJson(value, Likes::class.java)
    }

    //
    @TypeConverter
    fun fromListed(value: Listed?): String? {
        if(value == null) return null
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toListed(value: String?): Listed? {
        return Gson().fromJson(value, Listed::class.java)
    }

    //
    @TypeConverter
    fun fromMenu(value: Menu?): String? {
        if(value == null) return null
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toMenu(value: String?): Menu? {
        return Gson().fromJson(value, Menu::class.java)
    }

    //
    @TypeConverter
    fun fromPage(value: Page?): String? {
        if(value == null) return null
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toPage(value: String?): Page? {
        return Gson().fromJson(value, Page::class.java)
    }

    //
    @TypeConverter
    fun fromPageUpdates(value: PageUpdates?): String? {
        if(value == null) return null
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toPageUpdates(value: String?): PageUpdates? {
        return Gson().fromJson(value, PageUpdates::class.java)
    }

    //
    @TypeConverter
    fun fromPhotos(value: Photos?): String? {
        if(value == null) return null
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toPhotos(value: String?): Photos? {
        return Gson().fromJson(value, Photos::class.java)
    }

    //
    @TypeConverter
    fun fromPopular(value: Popular?): String? {
        if(value == null) return null
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toPopular(value: String?): Popular? {
        return Gson().fromJson(value, Popular::class.java)
    }

    //
    @TypeConverter
    fun fromPrice(value: Price?): String? {
        if(value == null) return null
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toPrice(value: String?): Price? {
        return Gson().fromJson(value, Price::class.java)
    }

    //
    @TypeConverter
    fun fromReasons(value: Reasons?): String? {
        if(value == null) return null
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toReasons(value: String?): Reasons? {
        return Gson().fromJson(value, Reasons::class.java)
    }

    //
    @TypeConverter
    fun fromSpecials(value: Specials?): String? {
        if(value == null) return null
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toSpecials(value: String?): Specials? {
        return Gson().fromJson(value, Specials::class.java)
    }

    //
    @TypeConverter
    fun fromStats(value: Stats?): String? {
        if(value == null) return null
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toStats(value: String?): Stats? {
        return Gson().fromJson(value, Stats::class.java)
    }

    //
    @TypeConverter
    fun fromTipsX(value: TipsX?): String? {
        if(value == null) return null
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toTipsX(value: String?): TipsX? {
        return Gson().fromJson(value, TipsX::class.java)
    }



    companion object {
        const val SEPARATOR = ";;"

        @TypeConverter
        @JvmStatic
        fun poisToString(pois: MutableList<Poi>?): String? =
            pois?.map { Gson().toJson(it) }?.joinToString(separator = SEPARATOR)

        @TypeConverter
        @JvmStatic
        fun stringToPois(pois: String?): MutableList<Poi>? =
            pois?.split(SEPARATOR)?.map { Gson().fromJson(it, Poi::class.java) }?.toMutableList()

        //
        @TypeConverter
        @JvmStatic
        fun categoriesToString(pois: MutableList<Category>?): String? =
            pois?.map { Gson().toJson(it) }?.joinToString(separator = SEPARATOR)

        @TypeConverter
        @JvmStatic
        fun stringToCategories(pois: String?): MutableList<Category>? =
            pois?.split(SEPARATOR)?.map { Gson().fromJson(it, Category::class.java) }?.toMutableList()

    }
}