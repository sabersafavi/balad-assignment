package com.saber.baladassessment.data.entities.poi_detail

data class Lists(
    val groups: List<Group>
)