package com.saber.baladassessment.data.entities.poi_detail

data class Contact(
    val facebook: String,
    val facebookName: String,
    val formattedPhone: String,
    val instagram: String,
    val phone: String
)