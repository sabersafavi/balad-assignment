package com.saber.baladassessment.data.entities.poi_detail

data class Popular(
    val isLocalHoliday: Boolean,
    val isOpen: Boolean,
    val timeframes: List<Timeframe>
)