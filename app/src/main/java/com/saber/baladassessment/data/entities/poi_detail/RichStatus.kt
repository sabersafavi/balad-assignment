package com.saber.baladassessment.data.entities.poi_detail

data class RichStatus(
    val entities: List<Any>,
    val text: String
)