package com.saber.baladassessment.di

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.saber.baladassessment.BuildConfig
import com.saber.baladassessment.data.local.AppDatabase
import com.saber.baladassessment.data.local.PoiDao
import com.saber.baladassessment.data.remote.FoursquareService
import com.saber.baladassessment.data.remote.HeaderInterceptor
import com.saber.baladassessment.data.remote.PoiRemoteDataSource
import com.saber.baladassessment.data.repository.PoiRepository
import com.saber.baladassessment.utils.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson): Retrofit {

        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)

        return Retrofit.Builder()
            .baseUrl(BuildConfig.SERVER_URL)
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(
                GsonConverterFactory.create(
                    GsonBuilder().serializeNulls().create()
                )
            )
            .client(
                OkHttpClient.Builder()
                    .addInterceptor(HeaderInterceptor())
                    .addInterceptor(logging)
                    .connectTimeout(Constants.CONNECT_TIME_OUT, TimeUnit.SECONDS)
                    .readTimeout(Constants.READ_TIME_OUT, TimeUnit.SECONDS)
                    .writeTimeout(Constants.WRITE_TIME_OUT, TimeUnit.SECONDS)
                    .build()
            )
            .build()
    }

    @Provides
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    fun providePoiService(retrofit: Retrofit): FoursquareService =
        retrofit.create(FoursquareService::class.java)

    @Singleton
    @Provides
    fun providePoiRemoteDataSource(characterService: FoursquareService) =
        PoiRemoteDataSource(characterService)

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context) =
        AppDatabase.getDatabase(appContext)

    @Singleton
    @Provides
    fun providePoiDao(db: AppDatabase) = db.poiDao()

    @Singleton
    @Provides
    fun provideRepository(
        remoteDataSource: PoiRemoteDataSource,
        localDataSource: PoiDao
    ) =
        PoiRepository(remoteDataSource, localDataSource)
}