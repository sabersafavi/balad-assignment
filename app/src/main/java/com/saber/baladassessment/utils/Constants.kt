package com.saber.baladassessment.utils

object Constants {
    const val CONNECT_TIME_OUT: Long = 30
    const val READ_TIME_OUT: Long = 30
    const val WRITE_TIME_OUT: Long = 30
    @JvmField
    var ACCESS_TOKEN = ""

    @JvmField
    var ORDER_SIZE = 0
}
